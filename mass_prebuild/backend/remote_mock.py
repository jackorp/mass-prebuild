""" Remote back-end implementation

    The remote back-end implementation makes use of the mock back-end.
    It verifies MPB is available on the remote system, send the configuration
    and all files needed, and execute the build locally.
"""

from pathlib import Path

import shutil
import subprocess
import time
import yaml

from .package import MpbPackage
from .backend import MpbBackend


class RemoteMockPackage(MpbPackage):
    """Mass pre-build remote package implementation

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_git(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_script(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_any()

    def _build_any(self):
        """Infrastructure specific build method"""
        # Since builds are taken care by the remote, no need to implement
        # anything locally.
        self.build_id = int(time.time_ns())

        self.check_status()

        self._update_package()

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        self._owner.monitor_project()

        if self.src_pkg_name not in self._owner.package_status:
            return False

        status = self._owner.package_status[self.src_pkg_name]['build_status']

        if self.build_status != status:
            self._logger.debug(f"New status for {self.name} is {status}")

        self.build_status = status
        self.build_id = self._owner.package_status[self.src_pkg_name]['build_id']
        self.committish = self._owner.package_status[self.src_pkg_name]['committish']
        self.src_pkg_name = self._owner.package_status.get('src_pkg_name', self.name)

        return True

    def cancel(self):
        """Infrastructure specific cancel method"""
        if self.build_status != 'SUCCESS':
            self.build_status = 'FAILED'
        # Cancellation managed by the remote

    def collect_data(self, dest, logs_only=False):
        """Infrastructure specific data collector method"""
        self._logger.debug(f'Collecting data (logs only? {logs_only}) for {self.name}')

    def clean(self):
        """Infrastructure specific clean method"""
        # Clean-up managed by the remote
        return

class RemoteMockBackend(MpbBackend):
    """Remote backend implementation"""
    # pylint: disable = too-many-instance-attributes
    def __init__(self, database, logger, config):
        """Call the super class, and initialize the mock build environment"""
        super().__init__(database, logger, config)

        self.tools = {
                'scp': {
                    'path': shutil.which("scp"),
                    'mandatory': True
                    },
                'ssh': {
                    'path': shutil.which("ssh"),
                    'mandatory': True
                    },
                }

        self._srcdir = None

        for name, tool in self.tools.items():
            if any([not tool['mandatory'], tool['path']]):
                continue
            raise RuntimeError(f'Unable to locate {name} to process build')

        self.build_path = Path(Path.home() / ".mpb" / "remote" / f"{self.build_id}")
        self.build_path.mkdir(parents=True, exist_ok=True)
        result_base = Path(self.build_path / 'result')
        result_base.mkdir(parents=True, exist_ok=True)

        self.config.setdefault('remote', {})
        self.config['remote'].setdefault('address', '127.0.0.1')
        if not self.base_build_id:
            self.config['checker'].setdefault('remote', {})
            self.config['checker']['remote'].setdefault('address', '127.0.0.1')

        self.package_status = {}
        self.remote_build_path = ""
        self.remote_build_id = self.config['remote'].get('remote_build_id', 0)

    def prepare(self):
        """Prepare the infrastructure with the main packages"""
        if not self.base_build_id:
            self.init_remote()

        super().prepare()

    def init_package_config(self, plist):
        """Init remote package configuration"""
        for pname, pkg in plist.items():
            if pkg['src_type'] != 'file':
                continue
            if '/' not in pkg['src']:
                continue
            plist[pname]['src'] = pkg['src'].split('/')[-1]

    def init_remote_conf(self):
        """Initialize remote configuration"""
        config = self.config.copy()

        config['backend'] = 'mock'

        self.init_package_config(self.config['packages'])

        for ptype in ['list', 'append']:
            if ptype in self.config['revdeps']:
                self.init_package_config(self.config['revdeps'][ptype])

        config.pop('data', None) # Reset to default value
        config.pop('database', None) # Reset to default value

        return config

    def _exec_commands(self, cmds):
        """Execute a list of commands and check their results"""

        for name, cmd in cmds.items():
            self._logger.debug(f'Executing {name} subprocess')

            result = subprocess.run(cmd['cmd'],
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT,
                                    cwd=cmd['cwd'],
                                    text=True,
                                    check=False)

            self._logger.debug(result.stdout)
            result.check_returncode()

        return result.stdout

    def upload_package_data(self, plist, cmds):
        """Prepare package data in the remote"""
        for pname, pkg in plist.items():
            if pkg['src_type'] != 'file':
                continue
            if '/' not in pkg['src']:
                continue
            cmds[f'scp-{pname}'] = {
                    'cmd': [self.tools['scp']['path'],
                            pkg['src'],
                            f"{self.config['remote']['address']}:{self.remote_build_path}/data"],
                    'cwd': str(self.build_path)
                    }

    def init_remote(self):
        """Initialize remote"""
        cmds = {
                'test-ssh': {
                    'cmd': [self.tools['ssh']['path'],
                            '-q',
                            '-o', 'BatchMode=yes',
                            '-o', 'StrictHostKeyChecking=no',
                            '-o', 'ConnectTimeout=5',
                            self.config['remote']['address'],
                            'exit 0'],
                    'cwd': str(self.build_path)
                    },
                'test-mpb': {
                    'cmd': [self.tools['ssh']['path'],
                            self.config['remote']['address'],
                            'which mpb'],
                    'cwd': str(self.build_path)
                    },
                'mkdir': {
                    'cmd': [self.tools['ssh']['path'],
                            self.config['remote']['address'],
                            f'mkdir -p ~/.mpb_remote/{self.build_id}/data'],
                    'cwd': str(self.build_path)
                    },
                'build_path': {
                    'cmd': [self.tools['ssh']['path'],
                            self.config['remote']['address'],
                            f'realpath ~/.mpb_remote/{self.build_id}'],
                    'cwd': str(self.build_path)
                    }
                }

        self.remote_build_path = self._exec_commands(cmds).strip()

        config = Path(self.build_path / 'mpb.config')
        with config.open(mode='w', encoding='utf-8') as file:
            print(yaml.dump(self.init_remote_conf()), file=file)

        cmds = {
                'scp-config': {
                    'cmd': [self.tools['scp']['path'],
                            str(config),
                            f"{self.config['remote']['address']}:{self.remote_build_path}/data"],
                    'cwd': str(self.build_path)
                    },
                }

        self.upload_package_data(self.config['packages'], cmds)

        for ptype in ['list', 'append']:
            if ptype in self.config['revdeps']:
                self.upload_package_data(self.config['revdeps'][ptype], cmds)

        cmds['mpb-build'] = {
                    'cmd': [self.tools['ssh']['path'],
                            self.config['remote']['address'],
                            f'mpb -d -c {self.remote_build_path}/data/mpb.config'],
                    'cwd': str(self.build_path)
                    }

        stdout = self._exec_commands(cmds).split()
        self.remote_build_id = int(stdout[-5])
        self.config['remote'].setdefault('remote_build_id', self.remote_build_id)

        self.checker_build.remote_build_id = int(stdout[-1])
        self.config['checker']['remote'].setdefault('remote_build_id',
                                                    self.checker_build.remote_build_id)

        self._database.save_config(self.build_id, self.config)

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return RemoteMockPackage

    def monitor_project(self):
        """Get the whole project's package data"""
        cmds = {
                'collect-info': {
                    'cmd': [self.tools['ssh']['path'],
                            self.config['remote']['address'],
                            f'mpb -b {self.remote_build_id} --info -VVV'],
                    'cwd': str(self.build_path)
                    }
                }

        content = self._exec_commands(cmds).split("Full package list:")[-1].split("--8<--")[1]
        packages = yaml.safe_load(content)

        if packages.get('packages'):
            self.package_status.update(packages['packages'])

        if 'revdeps' in packages:
            if packages['revdeps'].get('list', None):
                self.package_status.update(packages['revdeps']['list'])
            if packages['revdeps'].get('append', None):
                self.package_status.update(packages['revdeps']['append'])

        self._logger.debug(f'Retrieved {self.package_status}')

    def location(self):
        """Get URL of the project"""
        return f"{self.config['remote']['address']}:{self.remote_build_path}"

class RemoteDummyBackend(RemoteMockBackend):
    """Remote dummy backend implementation"""

    def init_remote_conf(self):
        """Initialize remote dummy configuration"""
        config = super().init_remote_conf()

        config['backend'] = 'dummy'

        return config
