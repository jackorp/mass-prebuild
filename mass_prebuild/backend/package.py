""" Generic package implementation

    This module provides the main functionalities for the mass pre-builder. The
    infrastructure specific packages should only implement the infrastructure
    specific actions, the overall logic is centralized here.
"""

import logging
import yaml

from .db import authorized_status, authorized_types
from ..utils.whatrequires import get_last_build

MAIN_PKG = 1 << 0
REV_DEP_PKG = 1 << 1
REV_DEP_PKG_APPEND = 1 << 2
NO_BUILD = 1 << 3

ALL_DEP_PKG = REV_DEP_PKG | REV_DEP_PKG_APPEND
ALL_PKG = MAIN_PKG | ALL_DEP_PKG


class MpbPackage:
    """Mass pre-build generic package handling helper

        This helper class is to be implemented by the infrastructure specific
        back-end.

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        nvr: name version release, information for the reporting
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    # pylint: disable = too-many-instance-attributes

    def __init__(
        self,
        database,
        logger,
        owner,
        pkg_data,
    ):
        """Create a new mass pre-build package

        The internals of the package are populated with the given data, unless
        the pkg_id is given. In the later case, the data is collected out of
        the database, based on the given package ID.
        Args:
            database: The database where data needs to be got from and stored to
            owner: The build owning this package
            pkg_data: The package data as a dictionary

        Returns:
            A new package object, filled with the required data.

        Raises:
            ValueError: if no package ID is given but mandatory parameters are
                missing.
            ValueError: If the given package ID doesn't match any entry in the
                database.
        """
        # pylint: disable = too-many-arguments, too-many-locals,
        # pylint: disable = too-many-branches, too-many-statements
        self._database = database
        self._logger = logger
        self._skip_archs = set()
        self._no_update = True

        _builders = {
            'distgit': self._build_distgit,
            'file': self._build_file,
            'git': self._build_git,
            'url': self._build_url,
            'script': self._build_script,
        }

        self.pkg_id = pkg_data['pkg_id']
        self.name = pkg_data['name']
        self._owner = owner
        self.base_build_id = owner.build_id
        self.build_id = pkg_data['build_id']
        self.build_status = 'FREE'
        self.pkg_type = pkg_data['pkg_type']
        self.skip_archs = pkg_data['skip_archs']
        self.src_type = pkg_data['src_type']
        self.src = pkg_data['src']
        self.committish = pkg_data['committish']
        self.priority = pkg_data['priority']
        self.nvr = ''
        self.after_pkg = pkg_data['after_pkg']
        self.with_pkg = pkg_data['with_pkg']
        self.retry = pkg_data['retry']
        self.retry_count = pkg_data['retry']

        self.archs = {a for a in self._owner.archs if a not in self.skip_archs}

        self._no_update = False

        if self.pkg_id:
            pkg = self._database.package_by_id(self.pkg_id)

            if pkg is None:
                raise ValueError(f'Package ID {self.pkg_id} doesn\'t exist')

            self._no_update = True
            for key in pkg.keys():
                try:
                    if key in ['archs', 'skip_archs']:
                        setattr(self, key, yaml.safe_load(pkg[key]))
                    else:
                        setattr(self, key, pkg[key])
                except AttributeError:
                    pass
            self._no_update = False

            if self.src_type not in _builders:
                raise ValueError(f'Source type unknown: {self.src_type}')

            self._builder = _builders[self.src_type]

            self.archs = {a for a in self._owner.archs if a not in self.skip_archs}
            self.src_pkg_name = self.name
            return

        if not pkg_data['name']:
            raise ValueError('No package ID given and name is empty')

        if self.src_type not in _builders:
            raise ValueError(f'Source type unknown: {self.src_type}')

        self._builder = _builders[self.src_type]

        self.pkg_id = self._update_package()
        self.src_pkg_name = self.name

    def _update_package(self):
        """Update package entry in the database"""
        if self._no_update:
            return self.pkg_id

        return self._database.update_package(
            {k.lstrip('_'): v for k, v in self.__dict__.items()}
        )

    def can_retry(self, pkg_list):
        """Return True if build can be reasonably retried"""
        if self.retry_count == 0:
            return False

        if self.retry_count > 0:
            return True

        # retry_count is negative, go with dynamic calculation.
        # The idea here is: if there is the previous priority doesn't have packages anymore, that
        # means that they all have failed. We therefore won't be able to find a successful build
        # order.
        can_retry = True
        prio = self.priority
        if prio > 0:
            prio_list = [p for p in pkg_list if p.priority == prio - 1]
            if not prio_list:
                can_retry = False
                self.retry_count = 0

        return can_retry

    def _build_distgit(self):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override _build_distgit')

    def _build_file(self):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override _build_file')

    def _build_git(self):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override _build_git')

    def _build_script(self):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override _build_script')

    def _build_url(self):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override _build_url')

    def build(self):
        """Generic caller for infrastructure specific build methods"""
        # pylint: disable = unused-variable
        if self.pkg_type & NO_BUILD:
            self.build_status = 'SUCCESS'
            return

        self.build_status = 'PENDING'

        self.src_pkg_name, committish, self.nvr = get_last_build(
            self.name,
            self._owner.distgit,
            self._owner.releasever,
            next(iter(self.archs)),  # Any arch should do the job
        )

        if self.committish == '@last_build':
            self.committish = ''
            if committish is not None:
                self.committish = committish

        if not self.committish:
            self.committish = self._owner.committish

        self._update_package()

        self._builder()

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        raise NotImplementedError('Must override build')

    def cancel(self):
        """Infrastructure specific cancel method"""
        raise NotImplementedError('Must override cancel')

    def collect_data(self, dest):
        """Infrastructure specific data collector method"""
        raise NotImplementedError('Must override collect_data')

    def clean(self):
        """Infrastructure specific clean method"""
        raise NotImplementedError('Must override clean')

    def info(self, level):
        """Print detailed information in a mpb config format"""
        # pylint: disable = too-many-branches
        prefix = '  '
        if self.pkg_type & ALL_DEP_PKG:
            prefix *= 2

        name = self.name
        if name.startswith('@'):
            name = f"'{name}'"

        committish = self.committish or ''
        if committish.startswith('@'):
            committish = f"'{committish}'"

        self._logger.log(level, f'{prefix}{name}:')
        if self.src_type != 'distgit':
            self._logger.log(level, f'{prefix}  src_type: {self.src_type}')
            self._logger.log(level, f'{prefix}  src: {self.src}')
        if self.src_pkg_name != self.name:
            self._logger.log(level, f'{prefix}  src_pkg_name: {self.src_pkg_name}')
        if committish != '':
            self._logger.log(level, f'{prefix}  committish: {committish}')
        if self.priority != 0:
            self._logger.log(level, f'{prefix}  priority: {self.priority}')

        if level < logging.ERROR:
            self._logger.log(level, f'{prefix}  build_id: {self.build_id}')
            self._logger.log(level, f'{prefix}  build_status: {self.build_status}')
            if self.archs:
                self._logger.log(level, f'{prefix}  archs: !!set')
                for arch in self.archs:
                    self._logger.log(level, f'{prefix}    {arch}: null')

        if self.skip_archs:
            self._logger.log(level, f'{prefix}  skip_archs: !!set')
            for skip_arch in self.skip_archs:
                self._logger.log(level, f'{prefix}    {skip_arch}: null')

        if self.retry:
            self._logger.log(level, f'{prefix}  retry: {self.retry}')
            self._logger.log(level, f'{prefix}  retry_count: {self.retry_count}')

    @property
    def build_status(self):
        """Return build status dictionary"""
        return self._build_status

    @build_status.setter
    def build_status(self, new_status):
        """Set the dictionary of build status

        Args:
            new_status: The status to be set

        Raises:
            ValueError: The status is not one of the authorized status
        """
        if new_status not in authorized_status:
            raise ValueError(f'Status value should be in {authorized_status}')

        self._build_status = new_status

        if self.pkg_id:
            self._update_package()

    @property
    def pkg_type(self):
        """Return the package type"""
        return self._pkg_type

    @pkg_type.setter
    def pkg_type(self, new_type):
        """Set the package type

        Args:
            new_type: The type to be set

        Raises:
            ValueError: Given type is not in {MAIN_PKG, REV_DEP_PKG,
            REV_DEP_PKG_APPEND}
        """
        if not new_type & ALL_PKG:
            raise ValueError(f'Unknown {new_type} package type')

        self._pkg_type = new_type

    @property
    def skip_archs(self):
        """Returns the set of unsupported archs"""
        return self._skip_archs

    @skip_archs.setter
    def skip_archs(self, new_archs):
        """Set the set of unsupported archs

            The set of supported archs is automatically updated by removing
            values from this unsupported arch set.
        Args:
            new_arch: The set to set

        Raises:
            TypeError: The input is not a set
            ValueError: The input contains unsupported values

        """
        if not new_archs:
            return

        if not isinstance(new_archs, set):
            raise TypeError('Set of arch must be given')

        if not all(elem in self._owner.archs for elem in new_archs):
            raise ValueError(f'Archs should be in {self._owner.archs}')

        self._skip_archs = new_archs.copy()

    @property
    def src_type(self):
        """Returns the source type"""
        return self._src_type

    @src_type.setter
    def src_type(self, new_src_type):
        """Sets the source type

        Args:
            new_src_type: The source type to set

        Raises:
            ValueError: The provided source type is not one of the supported
                source types

        """
        if new_src_type not in authorized_types:
            raise ValueError(f'Unknown source type {new_src_type}')

        self._src_type = new_src_type
