= mpb-report (1)
:doctype: manpage
:manmanual: mpb-report

== Name

mpb-report - generate a report on the given mass-prebuild

== Synopsis

*mpb-report* [_OPTION_]...

== Description

Once a mass-prebuild has been done, it may be useful to generate a report that could be shared with other developers.
That's where this tool comes handy, generating a summary of the build in markdown format, e.g.:

```
    $> mpb-report -b 190
```

This command will generate a report for the build 190, similar to the following:

```
# Mass-prebuild mpb.190 (ID:190)

This report was generated using mpb-report 0.7.0

## General information

Chroot: fedora-rawhide
Tested architectures: x86_64

Main packages tested:

    redhat-rpm-config:
        Source: (distgit) fedora
        NVR: redhat-rpm-config-250-1.fc39
        Commit/branch: 99fec129987025eae5b3b9054ca2c677e0ba5b7a

## Overall status

    47 out of 52 builds are done.
    Success: 30
    Under check: 5
    Manual confirmation needed: 9
    Failed: 8

## List of failed packages

    ceph:
        Source: (distgit) fedora
        NVR: ceph-17.2.5-11.fc39
        Commit/branch: 0d92b145fb7fe59f12495872ecaf8a12662ca745
    kernel-tools:
        Source: (distgit) fedora
        NVR: kernel-tools-6.2.0-0.rc8.git0.1.fc39
        Commit/branch: 0b76cfc632a478ec1d0cdf2d2da92a07acad8b14
    net-snmp:
        Source: (distgit) fedora
        NVR: net-snmp-5.9.3-2.fc38
        Commit/branch: 96bb8ca49d5e8db01cdba8c50ba73a8a51a1cecd
    xpra:
        Source: (distgit) fedora
        NVR: xpra-4.4.3-5.fc39
        Commit/branch: d44662aee52f6693b8a846ee4a9e995c24a567ad
    kernel:
        Source: (distgit) fedora
        NVR: kernel-6.2.0-0.rc8.20230217gitec35307e18ba.60.fc39
        Commit/branch: b6bcada4a761f6480600ec4fb7502d381d085014
    rpkg:
        Source: (distgit) fedora
        NVR: rpkg-1.65-3.fc38
        Commit/branch: 66220ebdda358f01ebc592d5bd5ee4c6375ef8e8
    fedpkg:
        Source: (distgit) fedora
        NVR: fedpkg-1.43-3.fc38
        Commit/branch: be3ff93dec8d7a81fbab44b1899b334043b5085b
    fedora-packager:
        Source: (distgit) fedora
        NVR: fedora-packager-0.6.0.7-2.fc38
        Commit/branch: 91adab39a626cc50733e44451f8eb558b2a1ca7c

## List of packages with unknown status

    python3.10:
        Source: (distgit) fedora
        NVR: python3.10-3.10.10-1.fc39
        Commit/branch: f7c068e6b95b5ee7e50db54d86b6dca91da98369
    python3.11:
        Source: (distgit) fedora
        NVR: python3.11-3.11.2-1.fc39
        Commit/branch: 0331299c7d019060c2dab6ab56c82e5f60aa4d2b
    python3.12:
        Source: (distgit) fedora
        NVR: python3.12-3.12.0~a5-1.fc39
        Commit/branch: 204e750d2e37ea1dd1d90c2da3b63b6f771b0f26
    python3.6:
        Source: (distgit) fedora
        NVR: python3.6-3.6.15-17.fc38
        Commit/branch: 055e00d4abba9da0551c19340d279cebcbb4603c
    python3.7:
        Source: (distgit) fedora
        NVR: python3.7-3.7.16-3.fc38
        Commit/branch: 9dd29da57e7d06da0e0e18db859c93f77a9ae2c9
    python3.8:
        Source: (distgit) fedora
        NVR: python3.8-3.8.16-3.fc38
        Commit/branch: fa8b4c2ac44b8b554b2b898f1e669e12f9dce6c5
    python3.9:
        Source: (distgit) fedora
        NVR: python3.9-3.9.16-3.fc38
        Commit/branch: 26dc60a272aa9f901ecf6a987d17bd64d43897a8
    root:
        Source: (distgit) fedora
        NVR: root-6.26.10-5.fc38
        Commit/branch: 1801745923d911ad2b2b0d529a86e2bdd8836fb3
    python2.7:
        Source: (distgit) fedora
        NVR: python2.7-2.7.18-30.fc38
        Commit/branch: 1a29dd3b0a66fc7ef8ca829a2733f4cd1341b2a8
```

// tag::options[]
== Options

*-h, --help*::
  Show the help message and exit.

*--verbose, -V*::
  Increase output verbosity.
  This option may be provided multiple times to modify the amount of information provided (e.g. *-VV*).
  Other options are affected by the verbosity level, and may provide different information depending on it.

*--version, -v*::
  Show the program version and exit.

*--buildid, -b* _BUILDID_::
  This option is to be used in order to specify which *mpb* build needs to be work on.

*--output, -o* _OUTPUT_::
  Where to store the report
// end::options[]

== Resources

More information about the mass-prebuilder can be found at https://gitlab.com/fedora/packager-tools/mass-prebuild.

== See also

*mass-prebuild*(7), *mpb*(1)
